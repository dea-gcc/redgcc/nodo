#!/usr/bin/env node
const SSB = require('ssb-server')
  .use(require('ssb-master')) // para que anden los rpcs
  .use(require('ssb-replicate')) // legacy replication (sino ebt no anda...)
  .use(require('ssb-lan')) // Ver peers en la red local
  .use(require('ssb-conn')) // Conexiones!
  .use(require('ssb-ebt')) // puedo replicar informacion de pares
  .use(require('ssb-backlinks')) // lo necesita about (es para ver links en mensajes)
  .use(require('ssb-friends')) // de quien replico data
  .use(require('ssb-private'))// para mensajes privados
  .use(require('ssb-threads')) // para armar bien una cadena de mensajes
  .use(require('ssb-about')) // acerca de
  .use(require('ssb-query')) // para hacer búsquedas en la db

const Config = require('ssb-config/inject')
const Client = require('ssb-client')
const muxrpcli = require('muxrpcli')
const cmdAliases = require('./aliases.js')

const fs = require('fs')
const path = require('path')

const args = process.argv.slice(2)
const cmd = args[0]

const opts = {
  caps: require('@redgcc/semilla'),
  port: 9999
}
const NAME = 'redgcc'
const DEFAULT_ROOT = path.join(process.env.HOME, '.' + NAME)

// onboarding
if (!fs.existsSync(DEFAULT_ROOT) && args.indexOf('--path') === -1) {
  console.log(`
  holi, es probable que sea la primera vez que me inicias. 
  Ahora se te va a crear una identidad que va a
  almacenar en la carpeta ${DEFAULT_ROOT}.
  Podrias elejir otra carpeta para almacenar data. 
  Eso lo podes hacer usando la opcion --path 'ruta'
  `)
}

const config = Config(NAME, opts)

// Este método tiene un par de inconsistencias
// Si corres el comando pelado te levanta el srv y te tira al panel (la fácil)
// También podes correr el comando "activar" (levantar el srv) y "panel" (ir al panel) por separado,
// con la config que quieras
// Ahora, si corres sin comando pero con config (path, port, etc...) no sabe como resolver.
// Así que si necesitas opciones custom tenes que correr los comandos explicitamente
//

// Activar el nodo
if (cmd === 'activar') {
  SSB(config)
  console.log('*** nodo activado ***')
} else if (!cmd) {
  console.log(`
    Holi, no me corriste con ningun comando,
    así que voy a levantar el nodo y mostrarte el panel
  `)
  SSB(config)
  require('@redgcc/panel')
} else if (cmd === 'panel') {
  require('@redgcc/panel')
} else {
// Mandarle mensajes
  Client(config.keys, config, (err, sbot) => {
    if (err) throw err

    sbot.manifest((err, manifest) => {
      if (err) throw err

      // add aliases
      for (var k in cmdAliases) {
        sbot[k] = sbot[cmdAliases[k]]
        manifest[k] = manifest[cmdAliases[k]]
      }

      muxrpcli(args, manifest, sbot, config.verbose)
    })
  })
}
